import { Component } from "react";

class Form extends Component{
    state = {
        name: this.props.draft.name,
        age: this.props.draft.age,
        // O componente deve receber essa prop chamada draft.
    };
    componentWillUnmount() {
    // Executar o método, recebido por prop, addDraft e passar o valor state "name" por parâmetro.
        this.props.addDraft({name:this.state.name,age:this.state.age});
    }

    handleAddUser = (name,age) => {
    // Executar o método, recebido por prop, addCustomer e passar o valor state "name" por parâmetro.
        console.log(name)
        this.props.addCustomer(name.toString(),age);
    };    

    render(){
        const {name,age} = this.state;
        return(
            <>
                <h1>{name}</h1>
                <h2>{age}</h2>
                <input type="text" 
                placeholder="Nome"
                value={name}
                onChange={(e) => {
                this.setState({name: e.target.value})
                }}/>

                <input type="number" 
                placeholder="Idade"
                value={age}
                onChange={(e) => {
                this.setState({age: e.target.value})
                }}/>
                
                <button onClick={() => {
                    this.handleAddUser(name,age)}}>Salvar</button>
                <button onClick={() => {
                    this.props.toggleOpened()}}>Fechar</button>
            </>
        );
    }
}

export default Form