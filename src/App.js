import './App.css';
import { Component } from 'react';
import Form from './components/Form';

class App extends Component {
  state = {
    costumers: [],
    opened: false,
    draft: ""
  };

  toggleOpened = () => {
    const {opened} = this.state;
    this.setState({opened: !opened})
  };

  addDraft = (draft) => {
    this.setState({draft: draft})
  };

  addCustomer = (name,age) =>{
    const {costumers} = this.state;
    const costumer = {name,age}
    this.setState({costumers: [...costumers, costumer]})
  };

  render(){
    const {costumers,opened,draft} = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <div>{
          costumers.map((costumer,index) => <>
          <h3 key={index+costumer.name}>{costumer.name}</h3>
          <h4 key={index+costumer.age}>{costumer.age}</h4>
          </>)
          }</div>
          {!opened ? <button onClick={() => this.toggleOpened()}>Novo</button> : 
          <Form draft={draft} addCustomer={this.addCustomer} addDraft={this.addDraft} toggleOpened={this.toggleOpened}/>}
    
        </header>
      </div>
    );
  }
}

export default App;
